import React, {useEffect} from 'react';
import {Collapse, Container, Input, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink,} from 'reactstrap';
import {connect} from "react-redux";
import {getMenus} from "../redux/action/adminMenusAction";
import {SITE_LANG} from "../tools/constant";
import {getLang, getText} from "../locales";

function Header(props) {
    useEffect(()=>{
        props.getMenus();
    },[]);

    const changeLang = (lang) => {
        localStorage.setItem(SITE_LANG, lang);
        window.location.reload();
    };
    return (
        <div className="header">
            <Navbar className="navbar-top" color="light" light expand="md">
                <Container>
                    <NavbarBrand href="/">
                        <img src="images/Dustlikdon.png" alt="home"/>
                    </NavbarBrand>
                    <NavbarToggler/>
                    <Collapse navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <NavLink href="#">
                                    <div className="position-relative input">
                                        <Input placeholder={getText('search')} type="text"/>
                                        <img style={{
                                            position: "absolute",
                                            left: "12px",
                                            top: "12px",
                                            cursor:"pointer"
                                        }} src="images/search.png" alt=""/>
                                    </div>
                                </NavLink>
                            </NavItem>
                            <div>
                                <NavItem className="d-flex">
                                    <a href="#" onClick={() => changeLang("ru")} className="nav-link text-decoration-none">RU</a>
                                    <a href="#" onClick={() => changeLang("uz")} className="nav-link text-decoration-none">UZ</a>
                                    <a href="#" onClick={() => changeLang("en")} className="nav-link text-decoration-none">EN</a>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="#">
                                        <button type="button" className="btn phone-btn btn-outline-success">
                                            <span className="icon icon-phone"/>
                                            99872 335-41-16
                                        </button>
                                    </NavLink>
                                </NavItem>
                            </div>
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
            <Navbar className="navbar-top navbar-bottom" color="light" light expand="md">
                <Container>
                    <NavbarBrand href="/">
                        <img  src="images/up.png" alt=""/>
                    </NavbarBrand>
                    <NavbarToggler/>
                    <Collapse navbar>

                        <Nav className="mr-auto" navbar>
                            {
                                props.menus.map((item,index)=>(
                                    <NavItem key={index}>
                                        <a className="nav-link" href="#">{getLang() === "ru" ? item.nameRu : getLang() === "en" ? item.nameEn : item.nameUz}</a>
                                    </NavItem>
                                ))
                            }


                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        </div>
    );
}
function mapStateToProps(state) {
    return {
        menus:state.menu.menus
    }
}

export default connect(mapStateToProps,{getMenus})(Header) ;