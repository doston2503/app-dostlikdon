import React from 'react';

function Services(props) {
    return (
        <div className="Services">
            <div className="container">
                <div className="row">
                    <div className="col-md">
                        <div className="picture">
                            <img src="images/image10.png" alt=""/>
                        </div>
                        <div className="none">data.gov.uz</div>
                        <div className="title">
                            O‘zbekiston Respublikasi
                            Prezidentining
                            rasmiy veb sayti
                        </div>
                    </div>
                    <div className="col-md">
                        <div className="picture">
                            <img src="images/image11.png" alt=""/>
                        </div>
                        <div className="none">data.gov.uz</div>
                        <div className="title">
                            Davlat interaktiv
                            xizmatlari yagona portali
                        </div>
                    </div>
                    <div className="col-md">
                        <div className="picture">
                            <img src="images/image10.png" alt=""/>
                        </div>
                        <div className="none">data.gov.uz</div>
                        <div className="title">
                            O‘zbekiston Respublikasi
                            Prezidentining
                            rasmiy veb sayti
                        </div>
                    </div>
                    <div className="col-md">
                        <div className="picture">
                            <img src="images/image13.png" alt=""/>
                        </div>
                        <div className="none">data.gov.uz</div>
                        <div className="title">
                            O‘zbekiston Respublikasi
                            Maktabgacha Ta'lim
                            Vazirligi
                        </div>
                    </div>
                    <div className="col-md">
                        <div className="picture">
                            <img src="images/image14.png" alt=""/>
                        </div>
                        <div className="none">data.gov.uz</div>
                        <div className="title">
                            “O’ZDONMAHSULOT”
                            aksiyadorlik
                            kompaniyasi
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Services;