import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter} from 'reactstrap';
import {connect} from "react-redux";
import {AvForm, AvField} from 'availity-reactstrap-validation';
import AdminLayout from "../pages/AdminLayout";
import {updateState} from "../redux/action/adminNewsAction";

class AdminNews extends Component {

    changeModal = () => {
        this.props.updateState({modalOpen: !this.props.modalOpen})
    };
    render() {

        return (
            <AdminLayout>
                <div className='admin-news'>
                    <div className="d-flex justify-content-between">
                        <div><h5>News</h5></div>
                        <div>
                            <Button type='button' color='success' onClick={this.changeModal}>Qo'shish</Button>
                        </div>
                    </div>


                    <Modal isOpen={this.props.modalOpen}
                           toggle={this.changeModal}>
                        <AvForm >
                            <ModalBody>
                                <AvField
                                    name="titleUz"
                                    type="text"
                                    label="News title (uz)"
                                    required
                                />
                                <AvField
                                    name="titleRu"
                                    type="text"
                                    label="News title (ru)"
                                    required
                                />

                                <AvField
                                    name="titleEn"
                                    type="text"
                                    label="News title (en)"
                                    required
                                />


                                <AvField
                                    name="descriptionUz"
                                    type="textarea"
                                    label="Description (uz)"
                                    required
                                />

                                <AvField
                                    name="descriptionRu"
                                    type="textarea"
                                    label="Description (ru)"
                                    required
                                />

                                <AvField
                                    name="descriptionEn"
                                    type="textarea"
                                    label="Description (en)"
                                    required
                                />

                                <label htmlFor="file">Yangilik uchun rasm</label>
                                <input type="file" id="file" className="form-control"/>


                            </ModalBody>
                            <ModalFooter>
                                <button type='submit' className='btn btn-success'>Add</button>
                                <button type='button' className='btn btn-secondary' onClick={this.changeModal}>Cancel</button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                </div>
            </AdminLayout>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        modalOpen: state.news.modalOpen,
    }
};

export default connect(mapStateToProps, {updateState})(AdminNews);