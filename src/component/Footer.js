import React from 'react';

function Footer(props) {
    return (
        <div className={'footer'}>
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <div className={'footer-title'}>
                            Dustlikdon.uz
                        </div>
                        <div className={'footer-address'}>
                            Jizzax viloyati Do‘stlik tumani
                            Sanoatchilar MFY Sanoat ko‘chasi 49
                        </div>
                    </div>
                    <div className="col-md-2">
                        <div className={'footer-info'}>
                            Asosiy
                        </div>
                        <div className={'footer-link'}><a href="#">Jamiyat haqida</a></div>
                        <div className={'footer-link'}><a href="#">Struktura</a></div>
                        <div className={'footer-link'}><a href="#">Yangiliklar</a></div>
                    </div>
                    <div className="col-md-3">
                        <div className={'footer-services'}>
                            Xizmatlar
                        </div>
                        <div className={'footer-link'}><a href="#">Elektron murojaatlar</a></div>
                        <div className={'footer-link'}><a href="#">Interaktiv xizmatlar</a></div>
                        <div className={'footer-link'}><a href="#">Saytdan izlash</a></div>

                    </div>
                    <div className="col-md-3">
                        <div className={'footer-connect'}>
                            Bog'lanish
                        </div>
                        <div className={'footer-link'}>
                            <span className={'mr-3'}>
                                <img src="images/footer-phone.png" alt="phone"/>
                            </span>
                            <a href="#">998 71 335 41 16</a>
                        </div>
                        <div className={'footer-link mt-3'}>
                            <span className={'mr-3'}>
                                <img src="images/footer-message.png" alt="message"/>
                            </span>
                            <a href="#">info@dustlikdon.uz</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default Footer;