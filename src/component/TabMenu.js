import React, {useState} from 'react';
import {
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Card,
    Button,
    CardTitle,
    CardText,
    Row,
    Col,
    CardImg, CardBody
} from 'reactstrap';
import classnames from 'classnames';


function TabMenu(props) {

    const [activeTab, setActiveTab] = useState('1');

    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }


    return (
        <div className="TabContent">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="main-title">
                            Media
                        </div>
                    </div>
                    <div className="col-md-12">
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({active: activeTab === '1'})}
                                    onClick={() => {
                                        toggle('1');
                                    }}
                                >
                                    Barchasi
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({active: activeTab === '2'})}
                                    onClick={() => {
                                        toggle('2');
                                    }}
                                >
                                    Foto galeriya
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({active: activeTab === '3'})}
                                    onClick={() => {
                                        toggle('3');
                                    }}
                                >
                                    Video galeriya
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </div>
                </div>
                <TabContent activeTab={activeTab}>
                    <TabPane tabId="1">
                        <Row>
                            <Col md={4}>
                                <Card>
                                    <CardImg src="images/card-img.png" alt=""/>
                                    <div className="camera"><img src="images/camera.png" alt=""/></div>
                                    <CardBody>
                                        <div className="title">
                                            <div>"DO`STLIKDONMAXSULOTLARI" AJ</div>
                                            <div>boshqaruv raisi...</div>
                                        </div>
                                        <div className="bottom">
                                            <div className="time">
                                                <img className="mr-2" src="images/bag.png" alt=""/>
                                                16:48 / 12.11.20
                                            </div>
                                            <div className="seen">
                                                <img className="mr-2" src="images/eye.png" alt=""/>
                                                321
                                            </div>
                                            <div className="message">
                                                <img className="mr-2" src="images/message.png" alt=""/>
                                                100
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md={4}>
                                <Card>
                                    <CardImg src="images/card-img.png" alt=""/>
                                    <div className="camera"><img src="images/camera.png" alt=""/></div>
                                    <CardBody>
                                        <div className="title">
                                            <div>"DO`STLIKDONMAXSULOTLARI" AJ</div>
                                            <div>boshqaruv raisi...</div>
                                        </div>
                                        <div className="bottom">
                                            <div className="time">
                                                <img className="mr-2" src="images/bag.png" alt=""/>
                                                16:48 / 12.11.20
                                            </div>
                                            <div className="seen">
                                                <img className="mr-2" src="images/eye.png" alt=""/>
                                                321
                                            </div>
                                            <div className="message">
                                                <img className="mr-2" src="images/message.png" alt=""/>
                                                100
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md={4}>
                                <Card>
                                    <CardImg src="images/card-img2.png" alt=""/>
                                    <div className="camera"><img src="images/camera.png" alt=""/></div>
                                    <CardBody>
                                        <div className="title">
                                            <div>"DO`STLIKDONMAXSULOTLARI" AJ</div>
                                            <div>boshqaruv raisi...</div>
                                        </div>
                                        <div className="bottom">
                                            <div className="time">
                                                <img className="mr-2" src="images/bag.png" alt=""/>
                                                16:48 / 12.11.20
                                            </div>
                                            <div className="seen">
                                                <img className="mr-2" src="images/eye.png" alt=""/>
                                                321
                                            </div>
                                            <div className="message">
                                                <img className="mr-2" src="images/message.png" alt=""/>
                                                100
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </TabPane>

                    <TabPane tabId="2">
                        <Row>
                            <Col md={4}>
                                <Card>
                                    <CardImg src="images/card-img2.png" alt=""/>
                                    <div className="camera"><img src="images/camera.png" alt=""/></div>
                                    <CardBody>
                                        <div className="title">
                                            <div>"DO`STLIKDONMAXSULOTLARI" AJ</div>
                                            <div>boshqaruv raisi...</div>
                                        </div>
                                        <div className="bottom">
                                            <div className="time">
                                                <img className="mr-2" src="images/bag.png" alt=""/>
                                                16:48 / 12.11.20
                                            </div>
                                            <div className="seen">
                                                <img className="mr-2" src="images/eye.png" alt=""/>
                                                321
                                            </div>
                                            <div className="message">
                                                <img className="mr-2" src="images/message.png" alt=""/>
                                                100
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md={4}>
                                <Card>
                                    <CardImg src="images/card-img.png" alt=""/>
                                    <div className="camera"><img src="images/camera.png" alt=""/></div>
                                    <CardBody>
                                        <div className="title">
                                            <div>"DO`STLIKDONMAXSULOTLARI" AJ</div>
                                            <div>boshqaruv raisi...</div>
                                        </div>
                                        <div className="bottom">
                                            <div className="time">
                                                <img className="mr-2" src="images/bag.png" alt=""/>
                                                16:48 / 12.11.20
                                            </div>
                                            <div className="seen">
                                                <img className="mr-2" src="images/eye.png" alt=""/>
                                                321
                                            </div>
                                            <div className="message">
                                                <img className="mr-2" src="images/message.png" alt=""/>
                                                100
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </TabPane>

                    <TabPane tabId="3">
                        <Row>
                            <Col md={4}>
                                <Card>
                                    <CardImg src="images/card-img.png" alt=""/>
                                    <div className="camera"><img src="images/camera.png" alt=""/></div>
                                    <CardBody>
                                        <div className="title">
                                            <div>"DO`STLIKDONMAXSULOTLARI" AJ</div>
                                            <div>boshqaruv raisi...</div>
                                        </div>
                                        <div className="bottom">
                                            <div className="time">
                                                <img className="mr-2" src="images/bag.png" alt=""/>
                                                16:48 / 12.11.20
                                            </div>
                                            <div className="seen">
                                                <img className="mr-2" src="images/eye.png" alt=""/>
                                                321
                                            </div>
                                            <div className="message">
                                                <img className="mr-2" src="images/message.png" alt=""/>
                                                100
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col md={4}>
                                <Card>
                                    <CardImg src="images/card-img.png" alt=""/>
                                    <div className="camera"><img src="images/camera.png" alt=""/></div>
                                    <CardBody>
                                        <div className="title">
                                            <div>"DO`STLIKDONMAXSULOTLARI" AJ</div>
                                            <div>boshqaruv raisi...</div>
                                        </div>
                                        <div className="bottom">
                                            <div className="time">
                                                <img className="mr-2" src="images/bag.png" alt=""/>
                                                16:48 / 12.11.20
                                            </div>
                                            <div className="seen">
                                                <img className="mr-2" src="images/eye.png" alt=""/>
                                                321
                                            </div>
                                            <div className="message">
                                                <img className="mr-2" src="images/message.png" alt=""/>
                                                100
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </TabPane>
                </TabContent>
            </div>
        </div>
    );
}

export default TabMenu;