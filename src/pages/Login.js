import React, {Component} from 'react';
import {AvForm, AvField} from 'availity-reactstrap-validation'
import {connect} from "react-redux";
import {loginUser} from "../redux/action/authAction";

class Login extends Component {

    login = (event,value) => {
       this.props.loginUser(value,this.props.history);

    };
    render() {
        return (

            <div className="container">
                <div className="row vh-100 align-items-center">
                    <div className="col-md-4 offset-4">
                        <div className="card">
                            <div className="card-header bg-success">
                                <h4 className="text-white text-center">Login page</h4>
                            </div>
                            <div className="card-body">
                                <AvForm onValidSubmit={this.login}>
                                    <AvField
                                        type='text'
                                        name="phoneNumber"
                                        placeholder="+99894"
                                        required
                                    />
                                    <AvField
                                        type='password'
                                        name="password"
                                        placeholder="Password"
                                        required
                                    />
                                    <button disabled={this.props.isLoader} type="submit" className="btn btn-block btn-success mt-3">
                                        {this.props.isLoader?
                                            <span className="spinner-border spinner-border-sm"/> :''
                                        }

                                        login
                                    </button>
                                </AvForm>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}
function getState(state) {

    return {
        isLoader:state.login.isLoader
    }
}

export default connect(getState,{loginUser})(Login);