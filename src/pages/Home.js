import React, {Component} from 'react';
import Header from "../component/Header";
import HeaderCarousel from "../component/HeaderCarousel";
import CompanyNews from "../component/CompanyNews";
import Footer from "../component/Footer";
import HomeNews from '../component/HomeNews'
import Complaint from "../component/Complaint";
import TabMenu from "../component/TabMenu";
import Services from "../component/Services";
class Home extends Component {
    render() {
        return (
            <div>
                <Header/>
               <HeaderCarousel/>
               <HomeNews/>
               <CompanyNews/>
               <Complaint/>
               <TabMenu/>
               <Services/>
               <Footer/>
            </div>
        );
    }
}

export default Home;