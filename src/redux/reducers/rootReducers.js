import {combineReducers} from "redux";
import {loginReducers} from "./loginReducer";
import {adminMenusReducer} from "./adminMenusReducer";
import {adminNewsReducer} from "./adminNewsReducer";

export const rootReducers=combineReducers({
  login:loginReducers,
  menu:adminMenusReducer,
  news: adminNewsReducer,
});