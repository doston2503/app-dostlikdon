import {UPDATE_STATE} from "../actionType/actionType";
import axios from "axios";
import {PATH_NAME} from "../../tools/constant";
import {toast} from "react-toastify";

export function updateState (data){
    return {
        type: UPDATE_STATE,
        payload: data
    }
}

export function addMenu(data){
    return function (dispatch){
        axios.post(PATH_NAME + "menu", data)
            .then((res) => {
                if (res.data.success){
                    toast.success(res.data.message);
                    dispatch(getMenus());
                    dispatch({
                        type: UPDATE_STATE,
                        payload: {
                            modalOpen: false,
                            selectedMenu: {}
                        }
                    })
                } else {
                    toast.error(res.data.message);
                }
            })
    }
}

export function getMenus(){
    return function (dispatch){
        axios.get(PATH_NAME + "menu")
            .then((res) => {
                dispatch(updateState({menus: res.data.data}))
            })
    }
}

export function getAllMenus(){
    return function (dispatch){
        axios.get(PATH_NAME + "menu/all")
            .then((res) => {
                dispatch(updateState({menus: res.data.data}))

            })
    }
}

export function deleteMenu(id){
    return function (dispatch){
        axios.delete(PATH_NAME + "menu/" + id)
            .then((res) => {
                if (res.data.success){
                    toast.success(res.data.message);
                    dispatch(getMenus());
                    dispatch({type: UPDATE_STATE, payload: {deleteModalOpen: false}})
                } else {
                    toast.error("Xatolik!");
                }
            })
    }
}
